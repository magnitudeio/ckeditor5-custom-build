/**
 * @license Copyright (c) 2014-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
 import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor.js';
 import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat.js';
 import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote.js';
 import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold.js';
 import CKFinder from '@ckeditor/ckeditor5-ckfinder/src/ckfinder.js';
 import CKFinderUploadAdapter from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter.js';
 import CloudServices from '@ckeditor/ckeditor5-cloud-services/src/cloudservices.js';
 import Code from '@ckeditor/ckeditor5-basic-styles/src/code';
 import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js';
 import Font from '@ckeditor/ckeditor5-font/src/font';
 import Heading from '@ckeditor/ckeditor5-heading/src/heading.js';
 import HtmlEmbed from '@ckeditor/ckeditor5-html-embed/src/htmlembed';
 import Image from '@ckeditor/ckeditor5-image/src/image.js';
 import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption.js';
 import ImageInline from '@ckeditor/ckeditor5-image/src/imageinline.js';
 import ImageResize from '@ckeditor/ckeditor5-image/src/imageresize.js';
 import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle.js';
 import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar.js';
 import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload.js';
 import Indent from '@ckeditor/ckeditor5-indent/src/indent.js';
 import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic.js';
 import Link from '@ckeditor/ckeditor5-link/src/link.js';
 import List from '@ckeditor/ckeditor5-list/src/list.js';
 import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed.js';
 import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph.js';
 import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
 import Table from '@ckeditor/ckeditor5-table/src/table.js';
 import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar.js';
 import TextTransformation from '@ckeditor/ckeditor5-typing/src/texttransformation.js';
 import SpecialCharacters from '@ckeditor/ckeditor5-special-characters/src/specialcharacters';
 import SpecialCharactersEssentials from '@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials';
 import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
 import Subscript from '@ckeditor/ckeditor5-basic-styles/src/subscript';
 import Superscript from '@ckeditor/ckeditor5-basic-styles/src/superscript';
 import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';

 // other imports
 import * as sanitizeHtml from 'sanitize-html';

 // constants for api auth
 const IFRAME_SRC = '//cdn.iframe.ly/api/iframe'
 const IFRAMELY_API_KEY = process.env.IFRAMELY_API_KEY
 
 class Editor extends ClassicEditor { }
 
 // Plugins to include in the build.
 Editor.builtinPlugins = [
	 Autoformat,
	 BlockQuote,
	 Bold,
	 CKFinder,
	 CKFinderUploadAdapter,
	 CloudServices,
	 Code,
	 Essentials,
	 Font,
	 Heading,
	 HtmlEmbed,
	 Image,
	 ImageCaption,
	 ImageInline,
	 ImageResize,
	 ImageStyle,
	 ImageToolbar,
	 ImageUpload,
	 Indent,
	 Italic,
	 Link,
	 List,
	 MediaEmbed,
	 Paragraph,
	 PasteFromOffice,
	 Table,
	 TableToolbar,
	 TextTransformation,
	 SpecialCharacters,
	 SpecialCharactersEssentials,
	 Strikethrough,
	 Subscript,
	 Superscript,
	 Underline
 ];
 
 Editor.defaultConfig = {
	 toolbar: {
		 items: [
			'heading', '|',
			'fontfamily', 'fontsize', '|',
			'alignment', '|',
			'fontcolor', 'fontbackgroundcolor', '|',
			'bold', 'italic', 'subscript', 'superscript','underline', 'code', 'strikethrough', '|',
			'link', '|',
			'bulletedList', 'numberedList', '|',
			'indent', 'outdent', '|',
			'specialCharacters', 'blockQuote', '|',
			'insertTable', '|',
			'imageUpload', 'mediaEmbed', 'htmlEmbed', '|',
			'undo','redo'
			] ,
		 shouldNotGroupWhenFull: true
		},
	 htmlEmbed: {
		 showPreviews: true,
		 sanitizeHtml: (inputHtml) => {
			 // Strip unsafe elements and attributes, e.g.:
			 // the `<script>` elements and `on*` attributes.
			 const outputHtml = sanitizeHtml(inputHtml);

			 return {
				 html: outputHtml,
				 // true or false depending on whether the sanitizer stripped anything.
				 hasChanged: true
			 };
		 }
	 },
	 image: {
		 // Configure the available styles.
		 styles: [
			 'alignLeft', 'alignCenter', 'alignRight', 'block', 'inline', 'alignBlockLeft', 'alightBlockRight'
		 ],
 
		 // Configure the available image resize options.
		 resizeOptions: [
			 {
				 name: 'resizeImage:original',
				 label: 'Original',
				 value: null
			 },
			 {
				 name: 'resizeImage:25',
				 label: '25%',
				 value: '25'
			 },
			 {
				 name: 'resizeImage:50',
				 label: '50%',
				 value: '50'
			 },
			 {
				 name: 'resizeImage:75',
				 label: '75%',
				 value: '75'
			 }
		 ],
 
		 // You need to configure the image toolbar, too, so it shows the new style
		 // buttons as well as the resize buttons.
		 toolbar: [ {
			 items: [
			 'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight', 'imageStyle:block', 'imageStyle:side'],
				 defaultItem: 'imageStyle:block'
			 },
			 '|',
			 'resizeImage',
			 '|',
			 'toggleImageCaption',
			 'imageTextAlternative',
			 'linkImage'
		 ]
	 },
	 table: {
		 contentToolbar: [
			 'tableColumn',
			 'tableRow',
			 'mergeTableCells'
		 ]
	 },
	 link: {
		decorators: {
			isExternal: {
				mode: 'manual',
				label: 'Open in a new tab',
				attributes: {
					target: '_blank'
				}
			}
		}
	},
	 // This value must be kept in sync with the language defined in webpack.config.js.
	 language: 'en'
 };
 
 export default Editor;
 